#!/usr/bin/env python

import pika, json

# Consumer variables
username     = 'username'
password     = 'password'
vhost        = 'vhost name' # This can be found in the credentials section of your customer portal
queue_name   = 'queue name' # e.g. consumer.telematics_heartbeat https://developer.platformscience.com/reference/telematics-heartbeat
ack_messages = True

# Connection string
connection_string = 'amqps://{}:{}@amqp.pltsci.com:5671/{}'.format(username, password, vhost)


# Create the connection to the RabbitMQ server
def createConnection(url):
    connection_string = pika.URLParameters(url)
    connection = pika.BlockingConnection(connection_string)
    channel = connection.channel()
    return channel

# Process messages from selected queue
def processMessages(channel, method, properties, body):
    print('{} message'.format(queue_name))
    print(json.loads(body))
    print('\n')

    # Save message to persistent storage then ack

try:
    print('Creating connection to RabbitMQ server')
    channel = createConnection(connection_string)
    
    print('Consuming Messages. To exit press CTRL+C\n')
    channel.basic_consume(
        queue=queue_name,
        on_message_callback=processMessages,
        auto_ack=ack_messages
    )
    channel.start_consuming()
    connection.close()
    
except KeyboardInterrupt:
    print('Interrupted')