# Python Example
This is an example Python consumer using the Pika library. This script continually pulls messages from the selected queue.

---

## Dependencies

* [Python 3](https://www.python.org/downloads/)
* Pip 3 (installed with Python 3)
* [Pika](https://pika.readthedocs.io/en/stable/)

---

## Running the script

* Run `pip3 install pika`
* Update `connection_url` and `queue_name` in `consumer.py`
* Run `python3 ./python/consumer.py`

---

## Resources

* [Pika](https://pika.readthedocs.io/en/stable/)
* [RabbitMQ Tutorial](https://www.rabbitmq.com/tutorials/tutorial-one-python.html)