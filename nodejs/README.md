# NodeJS Example
This is an example nodejs consumer using the [amqplib](http://www.squaremobius.net/amqp.node/) library. This script continually pulls messages from the selected queue.

---

## Dependencies

* [NodeJS](https://nodejs.org/en/)
* [amqplib](http://www.squaremobius.net/amqp.node/)

---

## Running the script

* Install [NodeJS](https://nodejs.org/en/download/)
* Run `npm install`
* Run `npm start`

---

## Resources

* [amqplib](http://www.squaremobius.net/amqp.node/)
* [RabbitMQ Tutorial](https://www.rabbitmq.com/tutorials/tutorial-one-javascript.html)