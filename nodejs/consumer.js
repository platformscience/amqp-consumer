const amqp = require('amqplib');

// Vars
const username    = 'guest';
const password    = 'guest';
const vhost       = 'vhost name'; // This can be found in the credentials section of your customer portal
const queueName   = `queue name`; // e.g. consumer.telematics_heartbeat https://developer.platformscience.com/reference/telematics-heartbeat
const ackMessages = true;

// Connection details
const connectionDetails = amqp.connect(`amqps://${username}:${password}@amqp.pltsci.com/${vhost}`);

// Consume messages
connectionDetails
    .then(connection => {
        return connection.createChannel();
    })
    .then(channel => {
        console.log(`Successfully connected to the RabbitMQ server`);
        console.log(`Consuming messages from ${queueName}`);
        return channel.consume(queueName, msg => {
            console.log(msg.content.toString());

            // Add logic to save messages to persistent storage

            // Ack messages once saved (https://www.rabbitmq.com/confirms.html)
            if (ackMessages) {
                channel.ack(msg);
            }
        });
    })
    .catch(err => {
        console.error(err);
    });
