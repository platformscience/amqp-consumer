# Platform Science Message Queue Consumer(s) #
This repository contains basic examples of consuming messages from the Platform Science message queues.

---

# Best Practices
Below is list of best practices for connecting to the Platform Science RabbitMQ message queues. If you are having trouble connecting and need assistance, please log into [theHub](https://thehub.platformscience.com/) and select Support to create a request.

## How to connect to a message queue

1. Format the [connection string](https://www.rabbitmq.com/uri-spec.html). Platform Science will provide the username, password and vhost (Platform Science subdomain)
    * Example connection string in production environments: 
        * `amqps://username:password@amqp.pltsci.com:5671/vhost`
    * Example connection string in pre-production environments: 
        * `amqps://username:password@amqp-staging.pltsci.com:5671/vhost`
    * Elements of a connection string:
        * Protocol
        * Username
        * Password
        * Host
        * Vhost (Subdomain)

2. Create a connection to the RabbitMQ server
    * [NodeJS](https://amqp-node.github.io/amqplib/channel_api.html#connect) - `amqp.connect(connection_string)`
    * [Python](https://pika.readthedocs.io/en/stable/modules/connection.html#pika.connection.Connection) - `pika.BlockingConnection(connection_string)`

3. Open a channel on the connection
    * [NodeJS](https://amqp-node.github.io/amqplib/channel_api.html#model_createChannel) - `connection.createChannel()`
    * [Python](https://pika.readthedocs.io/en/stable/modules/channel.html#pika.channel.Channel) - `connection.channel(connection, channel_number, on_open_callback)`

4. Consume messages
    * [NodeJS](https://amqp-node.github.io/amqplib/channel_api.html#channel_consume) - `consume(queue, callback, [options])`
    * [Python](https://pika.readthedocs.io/en/stable/modules/channel.html#pika.channel.Channel.basic_consume) - `channel.basic_consume(queue, on_message_callback, auto_ack=False, exclusive=False, consumer_tag=None, arguments=None, callback=None)`

## Best practices for reconnecting when disconnected

* Your application (client) should automatically reconnect if disconnected. Most libraries have functionality for catching errors with the connection.  In the case of a disconnect the solution is to catch this error and reconnect your application.

**Error Event Example**
```
connection.on('error', (err) => {
    // reconnect application here
})
```

## Acknowledging (`Acking`) messages best practices

* The consumer (client) is responsible for [Acknowledge](https://www.rabbitmq.com/amqp-0-9-1-reference.html#basic.ack) (`Acking`) each message.
* Each message should be Ackd once it is saved to persistent storage.

## Best practices for connections and channels

* When connecting to a message queue only use a single connection.
* Multiple channels, within the connection, can be used to consume messages quicker.

## BasicGet vs BasicConsume 

* We recommend using BasicConsume for best performance and efficiency. Read CloudAMQP's guide on this topic [here](https://www.cloudamqp.com/blog/rabbitmq-basic-consume-vs-rabbitmq-basic-get.html).


## Common Errors

* `operation queue.declare caused a channel exception access_refused:` - Clients are not able to write/declare queues to the vhost (subdomain). They are only allow to read queues from the vhost. This is commonly a problem when trying to gauge the depth of a queue (`channel.queue_declare`). Including `passive=True` to this method will resolve this error and provide the queue depth.
* `403, "ACCESS_REFUSED` - The credentials are incorrect or the user does not have permission to read from the selected queue.  If the user *should* have permission to the selected queue contact Platform Science and the permissions will be updated.

---

# Programming languages
Example consumers in a variety of languages.

* [Python](./python)
* [NodeJS](./nodejs)

---

# Resources

* [RabbitMQ](https://www.rabbitmq.com)
* [RabbitMQ Getting Started](https://www.rabbitmq.com/getstarted.html)
* [RabbitMQ Reference Guide](https://www.rabbitmq.com/amqp-0-9-1-reference.html)
* [CloudAMQP Tutorials](https://www.cloudamqp.com/blog/part1-rabbitmq-for-beginners-what-is-rabbitmq.html)
* [Platform Science Queue Documentation](https://developer.platformscience.com/reference/introduction)